#include "csapp.h"

typedef struct {
	char *buf;
	int n;
	int letters;
	int front;
	int rear;
	sem_t mutex;
	sem_t slots;
	sem_t items;
} sbuf_t;

void *productor(void *argv);
void *consumidor ();

sbuf_t buf;
int readcnt=0;
int volatile v=1;


void sbuf_init(sbuf_t *sp, int n)
{
	sp->buf = Calloc(n, sizeof(int));
	sp->n = n;
	sp->front = sp->rear = 0;
	Sem_init(&sp->mutex, 0, 1);
	Sem_init(&sp->slots, 0, n);
	Sem_init(&sp->items, 0, 0);
	sp->letters=0;
}

int main(int argc, char **argv)
{
	pthread_t pro,con;	
	char *filename;
	sbuf_init(&buf , 10);
	
	if (argc != 2) {
		printf("usage: %s <filename>\n", argv[0]);
		exit(0);
	}
	filename = argv[1];
	pthread_create(&pro , NULL, productor, filename);
	pthread_create(&con , NULL, consumidor, NULL);
	pthread_join(pro, NULL);
	pthread_join(con, NULL);
	
//
	
	return 0;

}

void sbuf_insert(sbuf_t *sp, char item)
{
	P(&sp->slots);
	P(&sp->mutex);
	sp->buf[(++sp->rear)%(sp->n)] = item;
	sp->letters++;
	V(&sp->mutex);
	V(&sp->items);
}

char sbuf_remove(sbuf_t *sp){
	int item;
	P(&sp->items);
	P(&sp->mutex);
	item = sp->buf[(++sp->front)%(sp->n)]; 
	sp->letters--;
	V(&sp->mutex);
	V(&sp->slots);
	return item;
}


void *productor(void *argv){
	char *filename = (char *) argv;
	int fd;
	char c;
	struct stat fileStat;

	if (stat(filename, &fileStat)<0){	
		printf("Archivo %s no fue encontrado\n",filename);
	}
	else{
		printf("Abriendo archivo %s...\n",filename);
		fd = Open(filename, O_RDONLY, 0);
		while(Read(fd,&c,1)){
			usleep(50);
			sbuf_insert(&buf,c);
		}			
		Close(fd);
		v=0;
	}
	return NULL;

}


void *consumidor (){
	 
	while(buf.letters>0 || v==1){
		sleep(1);
		printf("Char: %c\n", sbuf_remove(&buf));
	}

	return NULL;
}
